/* ------------------------------------------------------------------------------------------------------------ */
/* Context variables                                                                                            */
/* ------------------------------------------------------------------------------------------------------------ */
variable "namespace" {
  type = string
}

variable "stage" {
  type = string
}

variable "tags" {
  type        = map(string)
  description = "Key-value map of resource tags. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  default     = {}
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Common input to configute Flowlog resource                                                                   */
/* ------------------------------------------------------------------------------------------------------------ */
variable "traffic_type" {
  type        = string
  description = "The type of traffic to capture. Valid values: ACCEPT,REJECT, ALL"
  default     = "ALL"
}

variable "log_destination_type" {
  type        = string
  description = "The type of the logging destination. Valid values: cloud-watch-logs, s3"
  default     = "cloud-watch-logs"
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Input to configure CW Log group if it is used as Flowlog destination                                         */
/* ------------------------------------------------------------------------------------------------------------ */

variable "eni_id" {
  type        = string
  description = "Elastic Network Interface ID to attach to"
  default     = null
}

variable "subnet_id" {
  type        = string
  description = "Subnet ID to attach to"
  default     = null
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to attach to"
  default     = null
}

variable "log_format" {
  type        = string
  description = "The fields to include in the flow log record, in the order in which they should appear."
  default     = ""
}

variable "max_aggregation_interval" {
  type        = number
  description = "The maximum interval of time during which a flow of packets is captured and aggregated into a flow log record. Valid Values: 60 seconds (1 minute) or 600 seconds (10 minutes)"
  default     = 600
}

variable "cloudwatch_log_group_name" {
  type        = string
  description = "Name of the cloudwatch log group to be created if CloudWatch LogGroup is used as Flow Log destination"
  default     = "Flowlog"
}

variable "flow_log_iam_role_name" {
  type        = string
  description = "Name of the IAM role to be created. It allows to post flow logs to CloudWatch Log group"
  default     = "flowlog_role"
}

variable "flow_log_iam_policy_name" {
  type        = string
  description = "Name of the IAM policy to be created. It allows to post flow logs to CloudWatch Log group"
  default     = "flowlog_policy"
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "Number of days to retent log events in Cloudwatch Log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653, and 0. If you select 0, the events in the log group are always retained and never expire"
  default     = 0
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "The ARN of the KMS Key to use when encrypting log data"
  default     = null
}

variable "cloudwatch_log_tags" {
  type        = map(string)
  description = "Tags assigned to Cloudwatch log group"
  default     = {}
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Input to configure S3 Bucket if it is used as Flowlog destination                                            */
/* ------------------------------------------------------------------------------------------------------------ */
variable "s3_bucket_name" {
  type        = string
  description = "Name of the S3 Bucket to be created if S3 Bucket is used as Flow Log destination"
  default     = ""
}

variable "s3_bucket_force_destroy" {
  type        = string
  description = "Force destroy S3 Bucket"
  default     = false
}